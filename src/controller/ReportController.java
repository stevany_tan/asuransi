/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.File;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperPrintManager;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.view.JasperViewer;

/**
 *
 * @author edepe7
 */
public class ReportController {
    /**
     * Preview Laporan
     * @param namaReport
     * @param parameter
     * @param DataSource
     * @param JudulLap 
     */
    public static void PreviewLap(String namaReport, Map parameter, List DataSource, String JudulLap){
        try {
            JasperPrint jasperPrint = JasperFillManager.fillReport(namaReport, 
                    parameter, 
                    new JRBeanCollectionDataSource(DataSource) );
            JasperViewer jasperViewer = new JasperViewer(jasperPrint, false);
            jasperViewer.setVisible(true);
            jasperViewer.setTitle(JudulLap);
            
            
        } catch (JRException ex) {
            Logger.getLogger(ReportController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    public static void CetakLap(String namaLaporan, Map parameter, List dataSource, String JudulLap){
        try {
            
//            JRDataSource jRDataSource = new JRBeanCollectionDataSource(dataSource,false);
            //JasperPrint jasperPrint = JasperFillManager.fillReport(namaLaporan, parameter, jRDataSource );
            JasperPrint jasperPrint = JasperFillManager.fillReport(namaLaporan, parameter, new JRBeanCollectionDataSource(dataSource) );
            JasperPrintManager.printReport(jasperPrint, true);
            
        } catch (JRException ex) {
            Logger.getLogger(ReportController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
    }
    
}
