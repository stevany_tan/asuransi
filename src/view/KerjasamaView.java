/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;
import model.AsuransiModel;
import model.Dtl_KerjasamaModel;
import model.Hdr_KerjasamaModel;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import util.HibernateUtil;

/**
 *
 * @author user
 */
public class KerjasamaView extends javax.swing.JFrame {

    /**
     * Creates new form KerjasamaView
     */
    private DefaultTableModel modelTabelAsuransi ;

    public KerjasamaView() {
        initComponents();
        KondisiAwal();

    }

    private void KondisiAwal() {
        Calendar cal = Calendar.getInstance();


//        tglMulai.setDate(cal.getTime());
//        tglSelesai.setDate(cal.getTime());

        cmbCariAsuransi.removeAllItems();
        cmbCariAsuransi.addItem("Id");
        cmbCariAsuransi.addItem("Nama");
        cmbCariAsuransi.setSelectedIndex(1);
        
        CmbCariDetil.removeAllItems();
        CmbCariDetil.addItem("Id");
        CmbCariDetil.addItem("nama");
        CmbCariDetil.setSelectedIndex(1);
        
        cmbStatus.removeAllItems();
        cmbStatus.addItem("all");
        cmbStatus.addItem("Aktif");
        cmbStatus.addItem("Non Aktif");
        cmbStatus.setSelectedIndex(1);
        
        CmbStatusDetil.removeAllItems();
        CmbStatusDetil.addItem("all");
        CmbStatusDetil.addItem("aktif");
        CmbStatusDetil.addItem("non aktif");
        
        setTabelAsuransi();

        setLocationRelativeTo(this);

    }

    private void setTabelAsuransi() {
        SessionFactory session = HibernateUtil.getSessionFactory();
        Session sess = session.openSession();
        modelTabelAsuransi = new DefaultTableModel(){

            @Override
            
            public Class getColumnClass(int columnIndex) {
                return getValueAt(0, columnIndex).getClass();
            }

            @Override
            public int getRowCount() {
                return super.getRowCount();
            }

            @Override
            public int getColumnCount() {
                return super.getColumnCount();
            }

            @Override
            public String getColumnName(int column) {
                return super.getColumnName(column);
            }

            @Override
            public Object getValueAt(int row, int column) {
                return super.getValueAt(row, column);
            }
            
        };
        tblAsuransi.setModel(modelTabelAsuransi);

        modelTabelAsuransi.getDataVector().removeAllElements();
        modelTabelAsuransi.fireTableDataChanged();

        modelTabelAsuransi.addColumn("Id_Asuransi");
        modelTabelAsuransi.addColumn("Asuransi");
        modelTabelAsuransi.addColumn("Tgl Mulai");
        modelTabelAsuransi.addColumn("Tgl Selesai");
        modelTabelAsuransi.addColumn("Aktif");
        modelTabelAsuransi.addColumn("Keterangan");
        modelTabelAsuransi.addColumn("Syarat");
        modelTabelAsuransi.addColumn(" id ");

        tblAsuransi.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        tblAsuransi.getColumnModel().getColumn(0).setPreferredWidth(30);
        tblAsuransi.getColumnModel().getColumn(0).setMaxWidth(0);
        tblAsuransi.getColumnModel().getColumn(0).setMinWidth(0);
        tblAsuransi.getColumnModel().getColumn(1).setPreferredWidth(300);
        tblAsuransi.getColumnModel().getColumn(2).setPreferredWidth(100);
        tblAsuransi.getColumnModel().getColumn(3).setPreferredWidth(100);
        tblAsuransi.getColumnModel().getColumn(4).setPreferredWidth(80);
        tblAsuransi.getColumnModel().getColumn(5).setPreferredWidth(350);
        tblAsuransi.getColumnModel().getColumn(6).setPreferredWidth(350);
        tblAsuransi.getColumnModel().getColumn(7).setPreferredWidth(0);
        String q = " from Hdr_KerjasamaModel h inner join h.asuransiModel a where ";
        
        q = cmbCariAsuransi.getSelectedIndex() == 0 ? q + " a.id_asuransi like '" + txtCariAsuransi.getText().trim() + "%' " : q + " a.nama like '%" + txtCariAsuransi.getText().trim() + "%' ";
        switch (cmbStatus.getSelectedIndex() ){
            case 0 : 
                     break;
            case 1 : q = q + " and h.aktif = 1 ";    
                     break;   
            case 2 : q = q + " and h.aktif = 0 ";    
                     break;   
        }
//        if ( ChkTglMulai.isSelected()==true ){
//            q = q + " and tglMulai between '" + t +  "' and '' "
//        }
            
        Query query = sess.createQuery(q);
        Iterator it = query.iterate();
        while(it.hasNext()){
            Object row [] = (Object[]) it.next();
            Hdr_KerjasamaModel hdr = (Hdr_KerjasamaModel) row[0];
            AsuransiModel as = (AsuransiModel) row [1];
            Object[] o = new Object[8];
            o[0] = hdr.getAsuransiModel().getKd_asuransi();
            o[1] = as.getNama();
            o[2] = hdr.getTglMulai();
            o[3] = hdr.getTglSelesai();
            o[4] = ( hdr.getAktif()==1 ? "Aktif" :"Non Aktif ") ;
            o[5] = hdr.getKeterangan().trim();
            o[6] = hdr.getSyarat().trim();            
            o[7] = hdr.getId_kerjasama();
            modelTabelAsuransi.addRow(o);
        }
        

    }

    private void setKerjasamaDetil(Integer idKerjasamaCari ){
        
        DefaultTableModel TableModelDetil = new DefaultTableModel();
        
        TableModelDetil.getDataVector().removeAllElements();
        TableModelDetil.fireTableDataChanged();
        
        TableModelDetil.addColumn("id Perusahaan");
        TableModelDetil.addColumn("Nama Perusahaan");        
        TableModelDetil.addColumn("Tgl Mulai");
        TableModelDetil.addColumn("Tgl Selesai");
        TableModelDetil.addColumn("Status");
        TableModelDetil.addColumn("-id-");
        
        String queryString="select p.id_perusahaan, p.nama, d.TglMulai, d.TglSelesai, d.aktif   , d.id_kerjaSamaDetil from Dtl_KerjasamaModel d inner join d.perusahaanModel p " +
                " where d.hdr_kerjasama = " + idKerjasamaCari + " " ;
        
        SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
        Session session = sessionFactory.openSession();
        Query query = session.createQuery(queryString);
        Iterator iterator = query.iterate();
        while(iterator.hasNext() ){
            Object row [] = (Object[]) iterator.next();
            Object[] objects = new Object[6];
            objects[0] = row[0];
            objects[1] = row[1];
            objects[2] = row[2];
            objects[3] = row[3];
            objects[4] =(Boolean) row[4] == true  ? "Aktif":"Non Aktif";
            objects[5] = row[5];
//            TableModelDetil.addRow(row);
              TableModelDetil.addRow(objects);
        }
        tblPerusahaan.setModel(TableModelDetil);
        
    }
    
    private void setKerjasamaDetil(Integer idKerjasamaCari, String cari ){
        
        DefaultTableModel TableModelDetil = new DefaultTableModel();
        
        TableModelDetil.getDataVector().removeAllElements();
        TableModelDetil.fireTableDataChanged();
        
        TableModelDetil.addColumn("id Perusahaan");
        TableModelDetil.addColumn("Nama Perusahaan");        
        TableModelDetil.addColumn("Tgl Mulai");
        TableModelDetil.addColumn("Tgl Selesai");
        TableModelDetil.addColumn("Status");
        TableModelDetil.addColumn("-id-");
        
        String queryString="select p.id_perusahaan, p.nama, d.TglMulai, d.TglSelesai, d.aktif   , d.id_kerjaSamaDetil from Dtl_KerjasamaModel d inner join d.perusahaanModel p " +
                " where d.hdr_kerjasama = " + idKerjasamaCari + " " ;
        
        switch (CmbStatusDetil.getSelectedIndex()){
            case 0:
                    break;
            case 1: queryString =  queryString  + " and d.aktif = true ";
                    break;
            case 2: queryString =  queryString  + " and d.aktif = false ";
                    break;
        }
        if (CmbCariDetil.getSelectedIndex()==0){
            queryString =  queryString  + " and p.id_perusahaan like '" + TxtCariDetil.getText().trim()+ "%' ";
        }else{
            queryString =  queryString  + " and p.nama like '%" + TxtCariDetil.getText().trim()+ "%' ";
        }
        
        SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
        Session session = sessionFactory.openSession();
        Query query = session.createQuery(queryString);
        Iterator iterator = query.iterate();
        while(iterator.hasNext() ){
            Object row [] = (Object[]) iterator.next();
            Object[] objects = new Object[6];
            objects[0] = row[0];
            objects[1] = row[1];
            objects[2] = row[2];
            objects[3] = row[3];
            objects[4] =(Boolean) row[4] == true  ? "Aktif":"Non Aktif";
            objects[5] = row[5];
//            TableModelDetil.addRow(row);
              TableModelDetil.addRow(objects);
        }
        tblPerusahaan.setModel(TableModelDetil);
        
    }
    
    private void IsiDetil(Integer id_kerjasam_detil_Cari){
        String queryString="from Dtl_KerjasamaModel d  " +
                " where d.id_kerjaSamaDetil = " + id_kerjasam_detil_Cari + " " ;
        
        
        SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
        Session session = sessionFactory.openSession();        
        //Query query = session.createQuery(queryString);     
        //Iterator iterator = query.iterate();
        //Iterator iterator = criteria.list().iterator();
        
        Criteria criteria = session.createCriteria(Dtl_KerjasamaModel.class,"dtl_kerjasama");
        criteria.createAlias("dtl_kerjasama.perusahaanModel", "perusahaanModel");
        criteria.createAlias("dtl_kerjasama.hdr_kerjasama", "hdr_kerjasama");
        criteria.createAlias("hdr_kerjasama.asuransiModel", "asuransiModel");
        criteria.add(Restrictions.eq("dtl_kerjasama.id_kerjaSamaDetil", id_kerjasam_detil_Cari) );
        switch (CmbStatusDetil.getSelectedIndex()){
            case 0:
                    break;
            case 1: criteria.add(Restrictions.eq("dtl_kerjasama.status", true));
                    break;
            case 2: criteria.add(Restrictions.eq("dtl_kerjasama.status", false));
                    break;
        }
        if (CmbCariDetil.getSelectedIndex()==0){
            criteria.add(Restrictions.like("perusahaanModel.id_perusahaan", TxtCariDetil.getText().trim()+ "%"));
        }else{
            criteria.add(Restrictions.like("perusahaanModel.nama", "%" + TxtCariDetil.getText().trim()+ "%"));
        }
        
        Iterator iterator = criteria.list().iterator();
        if (iterator.hasNext()){
            Dtl_KerjasamaModel dtl_KerjasamaModel = (Dtl_KerjasamaModel) iterator.next();
//            lblTgl.setText(dtl_KerjasamaModel.getTglMulai().toString() + " s/d " + dtl_KerjasamaModel.getTglSelesai().toString());
            
            chkExcess.setSelected(dtl_KerjasamaModel.getExcess_b()==1? true:false);
            lblExcess.setText( "<html>" + dtl_KerjasamaModel.getExcess_s().trim()+  "</html>" );            
            
            
            chkRJ.setSelected(dtl_KerjasamaModel.getRj_b()==1? true:false);
            lblRJ.setText(dtl_KerjasamaModel.getRj_s().trim());            
            
            chkRI.setSelected(dtl_KerjasamaModel.getRi_b()==1? true:false);
            lblRI.setText(dtl_KerjasamaModel.getRi_s().trim());            
            
            chkRG.setSelected(dtl_KerjasamaModel.getRg_b()==1? true:false);
            lblRG.setText(dtl_KerjasamaModel.getRg_s().trim());
            
            chkVitamin.setSelected(dtl_KerjasamaModel.getVitamin_b()==1? true:false);
            lblVit.setText(dtl_KerjasamaModel.getVitamin_s().trim());            
            
            chkMelahirkan.setSelected(dtl_KerjasamaModel.getMelahirkan_b()==1? true:false);
            lblMelahirkan.setText(dtl_KerjasamaModel.getMelahirkan_s().trim());
            
            chkSupplemen.setSelected(dtl_KerjasamaModel.getSupplemen_b()==1? true:false);
            lblSupplemen.setText(dtl_KerjasamaModel.getSupplemen_s().trim());            
            
            chkMaternity.setSelected(dtl_KerjasamaModel.getMaternity_b()==1? true:false);
            lblMaternity.setText(dtl_KerjasamaModel.getMaternity_s().trim());
            
            chkMCU.setSelected(dtl_KerjasamaModel.getMcu_b()==1? true:false);
            lblMCU.setText(dtl_KerjasamaModel.getMcu_s().trim());
            
            lblKeterangan.setText(dtl_KerjasamaModel.getKet().trim());
            lblSyarat.setText(dtl_KerjasamaModel.getSyarat().trim());
        }
            
    }
    
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        cmbCariAsuransi = new javax.swing.JComboBox();
        txtCariAsuransi = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblAsuransi = new javax.swing.JTable();
        cmdRefresh = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        chkMaternity = new javax.swing.JCheckBox();
        chkRI = new javax.swing.JCheckBox();
        jLabel8 = new javax.swing.JLabel();
        chkExcess = new javax.swing.JCheckBox();
        chkVitamin = new javax.swing.JCheckBox();
        lblExcess = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        lblVit = new javax.swing.JLabel();
        chkSupplemen = new javax.swing.JCheckBox();
        jLabel10 = new javax.swing.JLabel();
        lblSupplemen = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        lblRI = new javax.swing.JLabel();
        lblRJ = new javax.swing.JLabel();
        chkRJ = new javax.swing.JCheckBox();
        jLabel14 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        chkMelahirkan = new javax.swing.JCheckBox();
        lblMelahirkan = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();
        chkRG = new javax.swing.JCheckBox();
        lblRG = new javax.swing.JLabel();
        jLabel20 = new javax.swing.JLabel();
        lblMaternity = new javax.swing.JLabel();
        jLabel21 = new javax.swing.JLabel();
        chkMCU = new javax.swing.JCheckBox();
        lblMCU = new javax.swing.JLabel();
        jLabel22 = new javax.swing.JLabel();
        jLabel23 = new javax.swing.JLabel();
        lblKeterangan = new javax.swing.JLabel();
        lblSyarat = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        cmbStatus = new javax.swing.JComboBox();
        jScrollPane2 = new javax.swing.JScrollPane();
        tblPerusahaan = new javax.swing.JTable();
        CmbCariDetil = new javax.swing.JComboBox();
        TxtCariDetil = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        CmbStatusDetil = new javax.swing.JComboBox();
        btnCariDtl = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        cmbCariAsuransi.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        tblAsuransi.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tblAsuransi.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblAsuransiMouseClicked(evt);
            }
        });
        tblAsuransi.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                tblAsuransiKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                tblAsuransiKeyTyped(evt);
            }
        });
        jScrollPane1.setViewportView(tblAsuransi);

        cmdRefresh.setText("Cari");
        cmdRefresh.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmdRefreshActionPerformed(evt);
            }
        });

        chkMaternity.setText("Ya");
        chkMaternity.setPreferredSize(new java.awt.Dimension(37, 17));

        chkRI.setText("Ya");
        chkRI.setPreferredSize(new java.awt.Dimension(37, 17));

        jLabel8.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        jLabel8.setText("Excess");

        chkExcess.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        chkExcess.setText("Ya");
        chkExcess.setPreferredSize(new java.awt.Dimension(37, 17));

        chkVitamin.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        chkVitamin.setText("Ya");
        chkVitamin.setPreferredSize(new java.awt.Dimension(37, 17));

        lblExcess.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        lblExcess.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblExcess.setText("ketExcess");
        lblExcess.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        lblExcess.setPreferredSize(new java.awt.Dimension(1071, 17));

        jLabel9.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        jLabel9.setText("Vitamin");

        lblVit.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        lblVit.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblVit.setText("ketVit");
        lblVit.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        chkSupplemen.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        chkSupplemen.setText("Ya");
        chkSupplemen.setPreferredSize(new java.awt.Dimension(37, 17));

        jLabel10.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        jLabel10.setText("Supplemen");

        lblSupplemen.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        lblSupplemen.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblSupplemen.setText("ketSupplemen");
        lblSupplemen.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel12.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        jLabel12.setText("Rawat Inap");

        lblRI.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        lblRI.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblRI.setText("ketRI");
        lblRI.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        lblRJ.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        lblRJ.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblRJ.setText("ketRJ");
        lblRJ.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        chkRJ.setText("Ya");
        chkRJ.setPreferredSize(new java.awt.Dimension(37, 17));

        jLabel14.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        jLabel14.setText("Rawat Jalan");

        jLabel16.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        jLabel16.setText("Melahirkan");

        chkMelahirkan.setText("Ya");
        chkMelahirkan.setPreferredSize(new java.awt.Dimension(37, 17));
        chkMelahirkan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chkMelahirkanActionPerformed(evt);
            }
        });

        lblMelahirkan.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        lblMelahirkan.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblMelahirkan.setText("ketMelahirkan");
        lblMelahirkan.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel19.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        jLabel19.setText("Rawat Gigi");

        chkRG.setText("Ya");
        chkRG.setPreferredSize(new java.awt.Dimension(37, 17));

        lblRG.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        lblRG.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblRG.setText("ketRG");
        lblRG.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel20.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        jLabel20.setText("Maternity");

        lblMaternity.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        lblMaternity.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblMaternity.setText("ketMaternity");
        lblMaternity.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel21.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        jLabel21.setText("MCU");

        chkMCU.setText("Ya");
        chkMCU.setPreferredSize(new java.awt.Dimension(37, 17));

        lblMCU.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        lblMCU.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblMCU.setText("ketMCU");
        lblMCU.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel22.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        jLabel22.setText("Keterangan");

        jLabel23.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        jLabel23.setText("Syarat");

        lblKeterangan.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        lblKeterangan.setText("keterangan");
        lblKeterangan.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        lblSyarat.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        lblSyarat.setText("syarat");
        lblSyarat.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel12, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel14)
                    .addComponent(jLabel19)
                    .addComponent(jLabel16, javax.swing.GroupLayout.PREFERRED_SIZE, 68, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel20, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel10)
                    .addComponent(jLabel21)
                    .addComponent(jLabel22)
                    .addComponent(jLabel23))
                .addGap(15, 15, 15)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(chkRI, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(chkRJ, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(chkRG, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(chkMelahirkan, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(chkMaternity, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(chkExcess, javax.swing.GroupLayout.PREFERRED_SIZE, 62, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(chkVitamin, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(chkSupplemen, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(chkMCU, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblRI, javax.swing.GroupLayout.PREFERRED_SIZE, 808, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblRJ, javax.swing.GroupLayout.PREFERRED_SIZE, 808, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblRG, javax.swing.GroupLayout.PREFERRED_SIZE, 808, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblMelahirkan, javax.swing.GroupLayout.PREFERRED_SIZE, 808, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblMaternity, javax.swing.GroupLayout.PREFERRED_SIZE, 831, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblExcess, javax.swing.GroupLayout.PREFERRED_SIZE, 831, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblVit, javax.swing.GroupLayout.PREFERRED_SIZE, 831, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblSupplemen, javax.swing.GroupLayout.PREFERRED_SIZE, 831, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblMCU, javax.swing.GroupLayout.PREFERRED_SIZE, 831, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(lblSyarat, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lblKeterangan, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        jPanel1Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {chkExcess, chkMCU, chkMaternity, chkMelahirkan, chkRG, chkRI, chkRJ, chkSupplemen, chkVitamin});

        jPanel1Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {lblExcess, lblMCU, lblMaternity, lblMelahirkan, lblRG, lblRI, lblRJ, lblSupplemen, lblVit});

        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(4, 4, 4)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel12)
                    .addComponent(chkRI, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblRI, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(jLabel14)
                    .addComponent(chkRJ, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblRJ, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(jLabel19)
                    .addComponent(chkRG, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblRG, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(jLabel16)
                    .addComponent(chkMelahirkan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblMelahirkan, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(jLabel20)
                    .addComponent(chkMaternity, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblMaternity, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(jLabel8)
                    .addComponent(chkExcess, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblExcess, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(jLabel9)
                    .addComponent(chkVitamin, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblVit, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(jLabel10)
                    .addComponent(chkSupplemen, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblSupplemen, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(jLabel21)
                    .addComponent(chkMCU, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblMCU, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel22)
                    .addComponent(lblKeterangan, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblSyarat, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel23)))
        );

        jLabel1.setText("Status");

        cmbStatus.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        tblPerusahaan.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tblPerusahaan.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblPerusahaanMouseClicked(evt);
            }
        });
        tblPerusahaan.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                tblPerusahaanKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                tblPerusahaanKeyTyped(evt);
            }
        });
        jScrollPane2.setViewportView(tblPerusahaan);

        CmbCariDetil.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        jLabel2.setText("Status");

        CmbStatusDetil.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        btnCariDtl.setText("Cari");
        btnCariDtl.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCariDtlActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1)
                    .addComponent(jScrollPane2)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(11, 11, 11)))
                .addGap(10, 10, 10))
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(CmbCariDetil, javax.swing.GroupLayout.PREFERRED_SIZE, 104, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(TxtCariDetil, javax.swing.GroupLayout.PREFERRED_SIZE, 342, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel2)
                        .addGap(31, 31, 31)
                        .addComponent(CmbStatusDetil, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(28, 28, 28)
                        .addComponent(btnCariDtl, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(cmbCariAsuransi, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(txtCariAsuransi, javax.swing.GroupLayout.PREFERRED_SIZE, 350, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 55, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cmbStatus, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(cmdRefresh, javax.swing.GroupLayout.PREFERRED_SIZE, 307, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(cmbCariAsuransi, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(txtCariAsuransi, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel1)
                        .addComponent(cmbStatus, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(cmdRefresh))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(CmbCariDetil, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(TxtCariDetil, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2)
                    .addComponent(CmbStatusDetil, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnCariDtl))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 97, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void cmdRefreshActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmdRefreshActionPerformed
        // TODO add your handling code here:
        setTabelAsuransi();
    }//GEN-LAST:event_cmdRefreshActionPerformed

    private void tblAsuransiMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblAsuransiMouseClicked
        // TODO add your handling code here:
         setKerjasamaDetil( (Integer) tblAsuransi.getValueAt(tblAsuransi.getSelectedRow() , 7)  );
        //JOptionPane.showMessageDialog(null, tblAsuransi.getValueAt(tblAsuransi.getSelectedRow() , 7)  );
    }//GEN-LAST:event_tblAsuransiMouseClicked

    private void tblPerusahaanMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblPerusahaanMouseClicked
        // TODO add your handling code here
        IsiDetil ( (Integer)  tblPerusahaan.getValueAt(tblPerusahaan.getSelectedRow(), 5)  );
        //JOptionPane.showMessageDialog(null, tblPerusahaan.getValueAt(tblPerusahaan.getSelectedRow(), 5) );
    }//GEN-LAST:event_tblPerusahaanMouseClicked

    private void tblAsuransiKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tblAsuransiKeyTyped
        // TODO add your handling code here:
    }//GEN-LAST:event_tblAsuransiKeyTyped

    private void tblPerusahaanKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tblPerusahaanKeyTyped
        // TODO add your handling code here:
        
        
         
    }//GEN-LAST:event_tblPerusahaanKeyTyped

    private void tblAsuransiKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tblAsuransiKeyReleased
        // TODO add your handling code here:
                setKerjasamaDetil( (Integer) tblAsuransi.getValueAt(tblAsuransi.getSelectedRow() , 7)  );

    }//GEN-LAST:event_tblAsuransiKeyReleased

    private void chkMelahirkanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chkMelahirkanActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_chkMelahirkanActionPerformed

    private void tblPerusahaanKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tblPerusahaanKeyReleased
        // TODO add your handling code here:
        IsiDetil ( (Integer)  tblPerusahaan.getValueAt(tblPerusahaan.getSelectedRow(), 5)  );
    }//GEN-LAST:event_tblPerusahaanKeyReleased

    private void btnCariDtlActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCariDtlActionPerformed
        // TODO add your handling code here:
        setKerjasamaDetil((Integer) tblAsuransi.getValueAt(tblAsuransi.getSelectedRow() , 7), "cari");
        
    }//GEN-LAST:event_btnCariDtlActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(KerjasamaView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(KerjasamaView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(KerjasamaView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(KerjasamaView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new KerjasamaView().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox CmbCariDetil;
    private javax.swing.JComboBox CmbStatusDetil;
    private javax.swing.JTextField TxtCariDetil;
    private javax.swing.JButton btnCariDtl;
    private javax.swing.JCheckBox chkExcess;
    private javax.swing.JCheckBox chkMCU;
    private javax.swing.JCheckBox chkMaternity;
    private javax.swing.JCheckBox chkMelahirkan;
    private javax.swing.JCheckBox chkRG;
    private javax.swing.JCheckBox chkRI;
    private javax.swing.JCheckBox chkRJ;
    private javax.swing.JCheckBox chkSupplemen;
    private javax.swing.JCheckBox chkVitamin;
    private javax.swing.JComboBox cmbCariAsuransi;
    private javax.swing.JComboBox cmbStatus;
    private javax.swing.JButton cmdRefresh;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JLabel lblExcess;
    private javax.swing.JLabel lblKeterangan;
    private javax.swing.JLabel lblMCU;
    private javax.swing.JLabel lblMaternity;
    private javax.swing.JLabel lblMelahirkan;
    private javax.swing.JLabel lblRG;
    private javax.swing.JLabel lblRI;
    private javax.swing.JLabel lblRJ;
    private javax.swing.JLabel lblSupplemen;
    private javax.swing.JLabel lblSyarat;
    private javax.swing.JLabel lblVit;
    private javax.swing.JTable tblAsuransi;
    private javax.swing.JTable tblPerusahaan;
    private javax.swing.JTextField txtCariAsuransi;
    // End of variables declaration//GEN-END:variables
}
